## botw_game_files

Used for https://github.com/zeldamaps/objmap .

Simply clone this into the public folder under the name game_files, then build the project using npm build. 

Serve the dist folder (can just be done with any server, no additional components needed).
