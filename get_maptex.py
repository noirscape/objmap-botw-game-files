import requests
from logzero import logger
from collections import namedtuple
import os
from functools import wraps

BASE_PATH = "temp_maptex"

def logged(f):
    @wraps(f)
    def wrapped(*args, **kwargs):
        logger.info('%s(%s)', f.__name__, ','.join([str(item) for item in args[1:]]))
        return f(*args, **kwargs)
    return wrapped

@logged
def download_url_to_file(url, sub_path):
    """
    Download a URL to a file.

    Returns the status code.
    """
    filename = os.path.join(BASE_PATH, sub_path)
    os.makedirs(os.path.split(filename)[0], exist_ok=True)

    # Thanks stackoverflow: https://stackoverflow.com/a/47103984/4666756
    r = requests.get(url, stream=True)
    with open(filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
    return r.status_code

MapZoomData = namedtuple('MapZoomData', ['z', 'x', 'y'])

upper_bound_data = [
    MapZoomData(z=0, x=0, y=0), # [0]
    MapZoomData(z=0, x=0, y=0), # [1]
    # BAD AND LAZY BUT EH
    MapZoomData(z=2, x=2, y=2),
    MapZoomData(z=3, x=5, y=4),
    MapZoomData(z=4, x=11, y=9),
    MapZoomData(z=5, x=23, y=19),
    MapZoomData(z=6, x=46, y=39),
    MapZoomData(z=7, x=93, y=78)
]

total = 0
counter = 0
for n in upper_bound_data:
    if n.z == 0:
        continue
    z = n.z
    x_list = list(range(n.x + 1))
    y_list = list(range(n.y + 1))
    for x in x_list:
        for y in y_list:
            total += 1

for n in range(2, 8):
    data = upper_bound_data[n]
    z = data.z
    x_list = list(range(data.x + 1))
    y_list = list(range(data.y + 1))
    for x in x_list:
        for y in y_list:
            counter += 1
            logger.info(f"Starting download ({counter}/{total})")
            arg1 = f"https://objmap.zeldamods.org/game_files/maptex/{z}/{x}/{y}.png"
            arg2 = f"{z}/{x}/{y}.png"
            failures = 0
            while failures < 5:
                try:
                    out = download_url_to_file(arg1, arg2)
                except:
                    logging.warning("Download failed, tyring again")
                    failures += 1
                    continue
                else:
                    break
            if failures >= 5:
                logging.exception("ERROR: DOWNLOAD FAILED TOO MANY TIMES! STOPPING APPLICATION, PLEASE CHECK YOUR SERVERS.")
                sys.exit(1)
            logger.info('%s(%s) -> %s', "download_url_to_file", ','.join([arg1, arg2]), out)
